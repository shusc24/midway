import React, { useCallback, Suspense } from 'react'
import './index.less'
import { message, Button } from 'antd'
import map from 'lodash/map'
import find from 'lodash/find'
import styled from '@emotion/styled'
import { useMount } from 'react-use'
import { observer, useLocalStore } from 'mobx-react'
import { action, observable } from 'mobx'
import MobxStore from '../../common/mobxStore'
import { rootInjector, tokens } from 'typed-inject'
import { HashRouter as Router, Route, Switch,Link } from 'react-router-dom'

const LoadingReadTest = React.lazy(() => import('./components/canRead'))

interface Props {
  news: NewsItem[]
}
interface NewsItem {
  id: string,
  title: string
}

class BtnModel extends MobxStore {
  @observable btnText = '123456'

  constructor (public name = 'btnModel') {
    super()
  }

  static inject = tokens('name')

  @action
  setBtnText = (val: string) => {
    this.btnText = val
  }
}

class Model extends MobxStore {
  @observable list = [
    {
      id: '1',
      title: '1 .Racket v7.3 Release Notes'
    },
    {
      id: '2',
      title: '2 .Free Dropbox Accounts Now Only Sync to Three Devices'
    },
    {
      id: '3',
      title: '3 .Voynich Manuscript Decoded by Bristol Academic'
    },
    {
      id: '4',
      title: '4 .Burger King to Deliver Whoppers to LA Drivers Stuck in Traffic'
    },
    {
      id: '5',
      title: '5 .How much do YouTube celebrities charge to advertise your product? '
    }
  ]

  BtnStore = rootInjector.provideValue('name', 'shusc24').injectClass(BtnModel)

  @action
  setList (id: string, title: string) {
    let findItem = find(this.list, { id })
    if (findItem) {
      findItem.title = title
    }
  }
}

const TestDiv = observer(styled(Button)`
  height: 60px;
  line-height: 60px;
  font-size: 22px;
`)

const Page: SFC<Props> = (props: Props): JSX.Element => {

  let store: Model = useLocalStore(() => {
    return Model.init(new Model())
  })

  const handleBtnClick = useCallback(() => {
    store.setList('1', '1234567899999')
    store.BtnStore.setBtnText('12345678999')
  }, [])

  useMount(() => {
    message.success('shusc24')
    console.log(store.BtnStore.name)
  })

  return (
    <div className='normal' >
      <div className='welcome' />
      <TestDiv onClick={handleBtnClick} size='small'>
        {store.BtnStore.btnText}
      </TestDiv>
      <ul className='list'>
        {
          map(store.list,(item: NewsItem) => (
            <li key={item.id}>
              <div>文章标题: {item.title}</div>
              <div className='toDetail'><Link to={`/news/${item.id}`}>点击查看详情</Link></div>
            </li>
          ))
        }
      </ul>
      <Router>
        <Route render={({ location }) => <Switch location={location}>
          <Route exact path='/' component={() => <Suspense fallback={<div>loading</div>}>
            <LoadingReadTest />
          </Suspense>} />
        </Switch> }>
        </Route>
      </Router>
    </div>
  )
}

Page.getInitialProps = (): Promise<Props> => {
  return Promise.resolve({
    news: [
      {
        id: '1',
        title: 'Racket v7.3 Release Notes'
      },
      {
        id: '2',
        title: 'Free Dropbox Accounts Now Only Sync to Three Devices'
      },
      {
        id: '3',
        title: 'Voynich Manuscript Decoded by Bristol Academic'
      },
      {
        id: '4',
        title: 'Burger King to Deliver Whoppers to LA Drivers Stuck in Traffic'
      },
      {
        id: '5',
        title: 'How much do YouTube celebrities charge to advertise your product? '
      }
    ]
  })
}
export default observer(Page)
